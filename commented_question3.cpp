//initializes a ROS 2 node that periodically publishes 
//"Hello, world!" messages on the specified topic using a timer-triggered callback
#include <chrono>
#include <functional>
#include <memory>
#include <string>
// Include necessary ROS 2 headers
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using namespace std::chrono_literals;

// Define a class named MinimalPublisher that inherits from rclcpp::Node
class MinimalPublisher : public rclcpp::Node
{
public:
// Constructor for the MinimalPublisher class
  MinimalPublisher()
  : Node("minimal_publisher"), count_(0)
  {
    // Create a publisher for std_msgs::msg::String messages on the topic "topic" with a queue size of 10
    publisher_ = this->create_publisher<std_msgs::msg::String>("topic", 10);
    
    // Create a timer that calls the timer_callback function every 500 milliseconds
    timer_ = this->create_wall_timer(
      500ms, std::bind(&MinimalPublisher::timer_callback, this));
  }

private:
  // Callback function called by the timer
  void timer_callback()
  {
    auto message = std_msgs::msg::String();
    message.data = "Hello, world! " + std::to_string(count_++);
     // Log a message to the console with the data being published
    RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
  size_t count_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalPublisher>());
  rclcpp::shutdown();
  return 0;
}
