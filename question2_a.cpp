#include <iostream>
#include <string>

class Student {
private:
    std::string name;
    int id;

public:
    // Constructor
    Student(std::string n, int i) : name(std::move(n)), id(i) {}

    // Getter for name
    std::string getName() const {
        return name;
    }

    // Getter for id
    int getId() const {
        return id;
    }

    // Setter for name
    void setName(const std::string& n) {
        name = n;
    }

    // Setter for id
    void setId(int i) {
        id = i;
    }
};

int main() {
    // Testing the Student class
    Student student("John Doe", 123);
    std::cout << "Name: " << student.getName() << ", ID: " << student.getId() << std::endl;

    // Setting new values
    student.setName("Jane Doe");
    student.setId(456);
    std::cout << "Name: " << student.getName() << ", ID: " << student.getId() << std::endl;

    return 0;
}
