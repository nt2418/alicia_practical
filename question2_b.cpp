#include <iostream>
#include <vector>

template <typename T>
void bubbleSort(std::vector<T>& list) {
    // Bubble Sort
    int n = list.size();
    for (int i = 0; i < n - 1; ++i) {
        for (int j = 0; j < n - i - 1; ++j) {
            // Swap if the element found is greater than the next element
            if (list[j] > list[j + 1]) {
                std::swap(list[j], list[j + 1]);
            }
        }
    }
}

int main() {
    // Testing the bubbleSort function with different types
    std::vector<int> intList = {5, 2, 8, 1, 6};
    bubbleSort(intList);
    std::cout << "Sorted intList: ";
    for (const auto& elem : intList) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;

    std::vector<float> floatList = {3.14, 2.71, 1.618, 0.577};
    bubbleSort(floatList);
    std::cout << "Sorted floatList: ";
    for (const auto& elem : floatList) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;

    std::vector<Student> studentList = {Student("Alice", 101), Student("Bob", 102), Student("Charlie", 103)};
    bubbleSort(studentList);
    std::cout << "Sorted studentList by name: ";
    for (const auto& student : studentList) {
        std::cout << student.getName() << " ";
    }
    std::cout << std::endl;

    return 0;
}
